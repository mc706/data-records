# Welcome to Data Record's Documentation

<table>
    <tr>
        <th>Documentation:</th>
        <td><a href="https://data-records.readthedocs.io/">https://data-records.readthedocs.io/</a></td>
    </tr>
    <tr>
        <th>Source Code:</th>
        <td><a href="https://gitlab.com/mc706/data-records">https://gitlab.com/mc706/data-records</a></td>
    </tr>
    <tr>
        <th>Issue Tracker:</th>
        <td><a href="https://gitlab.com/mc706/data-records/issues">https://gitlab.com/mc706/data-records/issues</a></td>
    </tr>
    <tr>
        <th>PyPI:</th>
        <td><a href="https://pypi.org/project/data-records/">https://pypi.org/project/data-records/</a></td>
    </tr>
</table>

Python lacks a Type-Checked Immutable Product Type for storing fields in a type safe way. In python3.7 `@dataclass(frozen=True)`
comes really close to providing this functionality to python, however it offers no defense at runtime of the types that
are declared. This may lead to a false confidence in developers code, as type checkers and IDEs will say that everything
works well together, but at runtime, the wrong type will be used resulting in a runtime error; exactly what types are
meant to try and prevent.

This library is an attempt at moving the type checking of data records  to the initialization step, allowing for 
errors to be handled at ingest instead of downstream during use. 

## Installation
This project is distributed via pypi. You can install it using `pip`. To get started:

```
pip install data-records
```
