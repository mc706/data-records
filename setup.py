"""
Package Setup
"""
import os
from setuptools import setup, find_packages

from src.data_records import __version__ as v

version = os.getenv("MODULE_VERSION_ID", v)

setup(
    name='data-records',
    description='Immutable Data Records with Type Coercion',
    long_description=open('README.md', 'r').read(),
    long_description_content_type='text/markdown',
    version=version,
    author='Ryan McDevitt',
    author_email='mcdevitt.ryan@gmail.com',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    url='https://gitlab.com/mc706/data-records',
    install_requires=[],
    extras_require={'dev': ['prospector', 'coverage', 'mypy', 'pytest', 'pytest-cov']},
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Software Development :: Libraries',
        'Topic :: Utilities',
    ],
    project_urls={
        'Docs': 'https://data-records.readthedocs.io/en/stable/#',
        'Source': 'https://gitlab.com/mc706/data-records',
        'Bug Reports/Issues': 'https://gitlab.com/mc706/data-records/issues'
    },
)
