# CHANGELOG

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a Changelog](http://keepachangelog.com/).



## Unreleased
---

### New

### Changes

### Fixes

### Breaks


## 0.4.2 - (2019-04-01)
---

### Changes
* add downloads badge

### Fixes
* Fix typos and examples in readme


## 0.4.1 - (2019-04-01)
---

### Fixes
* Use deepcopy to prevent mutable default problem


## 0.4.0 - (2019-03-31)
---

### New
* Allow for nested data records
* Add `._record_fields` property to indicate a class is a datarecord

### Changes
* Cleanup `coerce_type` for less complexity


## 0.3.0 - (2019-03-31)
---

### New
* add `__hash__` function generation
* add `__eq__` check
 
### Changes
* Update documentation


## 0.2.0 - (2019-03-31)
---

### New
* Add `.from_dict` classmethod 
* Add `.from_iter` classmethod


## 0.1.1 - (2019-03-31)
---

### New
* Add documentation and more doctests

### Changes
* Refactor into cleaner modules

### Fixes
* add `__all__` namespace back to init


## 0.1.0 - (2019-03-31)
---

### New
* Add replace method
* Add extract method

### Changes
* Expand README examples with doctestable samples


### Breaks
* Changed from base class to code generator


## 0.0.1 - (2019-03-28)
---

### New
* Initial release and tests
* Setup stub documentation


