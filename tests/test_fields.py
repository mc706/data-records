import unittest

from data_records.field import Field


class Foo:
    bar: str
    baz: str

    def test(self):
        return self.baz

class TestField(unittest.TestCase):
    def test_fields_list_for_class(self):
        field_list = Field.list_for_class(Foo)
        self.assertTrue(len(field_list) == 2)

    def test_fields_repr(self):
        field = Field('test', str, 'test')
        self.assertEqual(repr(field), "Field(test, <class 'str'>, test)")
