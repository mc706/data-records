import unittest

from data_records import datarecord

@datarecord
class Foo:
    """This class already has a docstring"""
    bar: str
    baz: str

    def __repr__(self):
        return f"<bar={self.bar}|baz={self.baz}>"


class TestEdgeCases(unittest.TestCase):
    def test_repr_untouched(self):
        test = Foo('a', 'b')
        self.assertEqual(repr(test), '<bar=a|baz=b>')

    def test_docstring_untouched(self):
        self.assertEqual(Foo.__doc__, "This class already has a docstring")
