import unittest
from decimal import Decimal
from typing import Optional, Any, List, Union, Set

from data_records import datarecord


@datarecord
class ExampleRecord:
    token: str
    created: float
    count: int
    data: Any
    comment: Optional[str] = None
    http_headers: Optional[str] = None
    sequence: Optional[int] = None
    extra: Optional[Any] = None
    element_ids: Optional[List[int]] = None
    stream_data: Optional[List[Union[float, str]]] = None
    name_set: Optional[Set[str]] = None



class TestDataRecord(unittest.TestCase):

    def setUp(self):
        self.data = {
            'token': 'abc123',
            'created': 12345.1,
            'count': 1,
            'comment': 'testing123',
            'data': 'test',
            'http_headers': 'ContentType: application-json',
            'sequence': 1,
            'extra': 'test'
        }

    def test_happy_path(self):
        record = ExampleRecord(**self.data)
        self.assertEqual(record.token, 'abc123')

    def test_missing_required_fields_raises_exception(self):
        del self.data['token']
        with self.assertRaises(TypeError):
            ExampleRecord(**self.data)

    def test_optional_fields_missing_succeeds(self):
        del self.data['comment']
        del self.data['http_headers']
        record = ExampleRecord(**self.data)
        self.assertEqual(record.comment, None)
        self.assertEqual(record.http_headers, None)

    def test_type_coercion(self):
        self.data['count'] = '1'
        self.data['created'] = '12345.1'
        record = ExampleRecord(**self.data)
        self.assertEqual(record.count, 1)
        self.assertEqual(record.created, 12345.1)

    def test_type_coercion_of_optional(self):
        self.data['sequence'] = '1'
        record = ExampleRecord(**self.data)
        self.assertEqual(record.sequence, 1)

    def test_type_coercion_failure(self):
        self.data['count'] = 'a'
        with self.assertRaises(ValueError):
            ExampleRecord(**self.data)

    def test_type_coercion_fails_optional(self):
        self.data['sequence'] = 'a'
        with self.assertRaises(ValueError):
            ExampleRecord(**self.data)

    def test_empty_strings_are_none(self):
        self.data['comment'] = ""
        self.data['sequence'] = ""
        record = ExampleRecord(**self.data)
        self.assertEqual(record.comment, None)
        self.assertEqual(record.sequence, None)

    def test_any_fields_are_required(self):
        del self.data['data']
        with self.assertRaises(TypeError):
            ExampleRecord(**self.data)

    def test_optional_any_not_required(self):
        del self.data['extra']
        record = ExampleRecord(**self.data)
        self.assertEqual(record.extra, None)

    def test_falsy_values_do_not_cast_to_none(self):
        self.data['count'] = 0
        record = ExampleRecord(**self.data)
        self.assertEqual(record.count, 0)

    def test_coercion_of_lists(self):
        self.data['element_ids'] = '[1,2,3,4,200]'
        record = ExampleRecord(**self.data)
        self.assertTrue(isinstance(record.element_ids, list))
        for element in [1, 2, 3, 4, 200]:
            self.assertTrue(element in record.element_ids)

    def test_coercion_of_mixed_lists(self):
        self.data['element_ids'] = '[1.1,2,3.0,4.5]'
        record = ExampleRecord(**self.data)
        self.assertTrue(isinstance(record.element_ids, list))
        self.assertTrue(all(map(lambda x: isinstance(x, int), record.element_ids)))

    def test_coercion_of_mixed_typed_lists(self):
        self.data['stream_data'] = "[1.1, 'a', 3, 'stuff']"
        record = ExampleRecord(**self.data)
        self.assertNotEqual(record.stream_data, None)
        first, second, third, fourth = record.stream_data
        self.assertEqual(first, 1.1)
        self.assertEqual(second, 'a')
        self.assertEqual(third, 3.0)
        self.assertEqual(fourth, 'stuff')

    def test_coercion_of_sets_lists(self):
        self.data['element_ids'] = {Decimal('1'), Decimal('2'), Decimal('3')}
        record = ExampleRecord(**self.data)
        first, second, third = record.element_ids
        self.assertEqual(first, 1)
        self.assertEqual(second, 2)
        self.assertEqual(third, 3)

    def test_coercion_of_sets_from_lists(self):
        self.data['name_set'] = ['Bill', 'Bob', 'Bill']
        record = ExampleRecord(**self.data)
        self.assertTrue({'Bill', 'Bob'} == record.name_set)

    def test_coercion_of_sets_from_sets(self):
        self.data['name_set'] = {'Bill', 'Bob'}
        record = ExampleRecord(**self.data)
        self.assertTrue({'Bill', 'Bob'} == record.name_set)

    def test_failed_coercion_of_broken_list(self):
        self.data['stream_data'] = "[1.1, 'a', 3, 'stuff'"
        with self.assertRaises(ValueError):
            ExampleRecord(**self.data)

    def test_coercion_of_set_from_list_string(self):
        self.data['name_set'] = "['Bill', 'Bob', 'Bill']"
        record = ExampleRecord(**self.data)
        self.assertTrue({'Bill', 'Bob'} == record.name_set)

    def test_coercion_of_set_from_set_string(self):
        self.data['name_set'] = "{'Bill', 'Bob', 'Bill'}"
        record = ExampleRecord(**self.data)
        self.assertTrue({'Bill', 'Bob'} == record.name_set)

    def test_failed_coercion_of_set_from_list_string(self):
        self.data['name_set'] = "['Bill', 'Bob', 'Bill'"
        with self.assertRaises(ValueError):
            ExampleRecord(**self.data)

    def test_failed_coercion_of_set_from_set_string(self):
        self.data['name_set'] = "{'Bill', 'Bob', 'Bill'"
        with self.assertRaises(ValueError):
            ExampleRecord(**self.data)
