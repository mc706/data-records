#!/bin/bash
current_version=$(cl current)
release_version=$(cl suggest)

sed -i '' "s/$current_version/$release_version/g" src/data_records/__init__.py

cl release --yes

git checkout -b release/${release_version}
git add CHANGELOG.md
git add src/data_records/__init__.py
git commit -m "Updating for release ${release_version}" -m "This is an automatically generated commit message prior to tagging a release."
git log -1 HEAD

echo "To revert the commit done by this file use: 'git reset HEAD~' and to revert file changes use: 'git reset --hard HEAD'"


